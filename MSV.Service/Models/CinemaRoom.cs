﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSV.Service.Models
{
    public class CinemaRoom
    {
        public int Id { get; set; }

        public String Name { get; set; }

        public int Rows { get; set; }

        public int SeatsPerRow { get; set; }

        public int AllowedDistance { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is CinemaRoom)
            {
                return (obj as CinemaRoom).Id == Id;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
