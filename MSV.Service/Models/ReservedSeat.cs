﻿using System;

namespace MSV.Service.Models
{
    public class ReservedSeat
    {
        public long Id { get; set; }

        public CinemaRoom Room { get; set; }

        public int RowNumber { get; set; }

        public int SeatNumber { get; set; }

        public DateTime ReservedTime { get; set; }
    }
}
