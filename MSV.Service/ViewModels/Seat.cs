﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSV.Service.ViewModels
{
    public class Seat
    {
        public int RowNumber { get; set; }

        public int SeatNumber { get; set; }
    }
}
