﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSV.Service.ViewModels
{
    public class ResultResponse
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }

        public static ResultResponse Success()
        {
            return new ResultResponse { IsSuccess = true };
        }

        public static ResultResponse Failure(string errorMessage)
        {
            return new ResultResponse { IsSuccess = false, Message = errorMessage };
        }
    }
}
