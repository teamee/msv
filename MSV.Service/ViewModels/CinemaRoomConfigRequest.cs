﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSV.Service.ViewModels
{
    public class CinemaRoomConfigRequest
    {
        public string Name { get; set; }
        public int Rows { get; set; }
        public int SeatsPerRow { get; set; }
        public int AllowedDistance { get; set; }
    }
}
