﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSV.Service.ViewModels
{
    public class ReserveSeatsRequest
    {
        public string RoomName { get; set; }
        public Seat[] Seats { get; set; }
    }
}
