﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSV.Service.Services
{
    public interface ICacheService
    {
        T GetItem<T>(String key);

        void UpdateItem<T>(String key, T item);

        void RemoveItem<T>(String key);
    }
}
