﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSV.Service.Services
{
    public interface ICinemaService
    {
        Task ConfigCinemaRoom(string name, int rows, int seatsPerRow, int allowedDistance);
        Task<IList<SeatInfo>> GetAvailableSeats(string roomName, int neededSeatCount);
        Task ReserveSeats(string roomName, IList<SeatInfo> needReservingSeats);
    }
}
