﻿using MSV.Service.Models;
using MSV.Service.Repository;
using ShareLib.Utility;
using System.Threading.Tasks;
using MSV.Service.Services.Impl.Seat;
using System.Linq;
using System.Collections.Generic;
using MSV.Service.Exceptions;
using System;

namespace MSV.Service.Services.Impl
{
    public class CinemaService : BaseService, ICinemaService
    {
        private readonly ICacheService _cacheService;
        private readonly ICinemaRoomRepository _cinemaRoomRepository;
        private readonly IReservedSeatRepository _reservedSeatRepository;

        public CinemaService(ICacheService cacheService, 
                             ICinemaRoomRepository cinemaRoomRepository, IReservedSeatRepository reservedSeatRepository)
        {
            _cacheService = cacheService;
            _cinemaRoomRepository = cinemaRoomRepository;
            _reservedSeatRepository = reservedSeatRepository;
        }

        public async Task ConfigCinemaRoom(string name, int rows, int seatsPerRow, int minDistance)
        {
            Guards.NotNullOrEmpty(name, "Room name must be not empty");
            Guards.MustBePositive(rows, "Row count must be positive");
            Guards.MustBePositive(seatsPerRow, "Seat count must be positive");
            Guards.MustBePositive(minDistance, "Min distance must be positive");

            CinemaRoom cinemaRoom = _cinemaRoomRepository.GetByName(name);
            if (cinemaRoom == null)
            {
                cinemaRoom = new CinemaRoom
                             {
                                Name = name,
                                Rows = rows,
                                SeatsPerRow = seatsPerRow,
                                AllowedDistance = minDistance
                             };
            }
            else
            {
                LockForUpdate(cinemaRoom);

                cinemaRoom.Name = name;
                cinemaRoom.Rows = rows;
                cinemaRoom.SeatsPerRow = seatsPerRow;
                cinemaRoom.AllowedDistance = minDistance;
            }

            _cinemaRoomRepository.Save(cinemaRoom);
            _reservedSeatRepository.DeleteByRoom(cinemaRoom);

            SeatMap seatMap = RebuildSeatMap(cinemaRoom);
            _cacheService.UpdateItem(cinemaRoom.Name, seatMap);
        }

        private SeatMap RebuildSeatMap(CinemaRoom room)
        {
            SeatMap seatMap = new SeatMap(room);
            var reservedSeats = _reservedSeatRepository.FindByRoom(room);
            if (reservedSeats != null && reservedSeats.Count > 0)
                seatMap.MarkSeatReserved(reservedSeats.Select(s => new SeatInfo
                                                                   {
                                                                       RowNumber = s.RowNumber,
                                                                       SeatNumber = s.SeatNumber
                                                                   })
                                                      .ToList());

            return seatMap;
        }

        public async Task<IList<SeatInfo>> GetAvailableSeats(string roomName, int neededSeatCount)
        {
            Guards.NotNullOrEmpty(roomName, "Room name must be not empty");
            Guards.MustBePositive(neededSeatCount, "Seat count must be positive");

            var cinemaRoom = _cinemaRoomRepository.GetByName(roomName);
            if (cinemaRoom == null)
                throw new CinemaException("Room doesn't exist.");

            SeatMap seatMap = GetSeatMap(cinemaRoom);
            var availableSeats = seatMap.GetAvailableSeats();

            if (availableSeats == null || availableSeats.Count < neededSeatCount)
                throw new CinemaException("There are less than " + neededSeatCount + " available seats");

            return availableSeats;
        }

        private SeatMap GetSeatMap(CinemaRoom room)
        {
            var seatMap = _cacheService.GetItem<SeatMap>(room.Name);

            if (seatMap == null)
            {
                seatMap = RebuildSeatMap(room);
                _cacheService.UpdateItem(room.Name, seatMap);
            }

            return seatMap;
        }

        public async Task ReserveSeats(string roomName, IList<SeatInfo> needReservingSeats)
        {
            CinemaRoom cinemaRoom = _cinemaRoomRepository.GetByName(roomName);
            if (cinemaRoom == null)
                throw new CinemaException("Room doesn't exist.");

            LockForUpdate(cinemaRoom);
            SeatMap seatMap = GetSeatMap(cinemaRoom);

            var unavailableSeats = seatMap.FetchUnavailableSeats(needReservingSeats);
            if (unavailableSeats != null && unavailableSeats.Count > 0)
            {          
                string unavailableSeatsText = string.Join(", ", 
                                                          unavailableSeats.Select(s => $"[{s.RowNumber}, {s.SeatNumber}]"));

                throw new CinemaException("Seats are not available for reserving: " + unavailableSeatsText);
            }

            foreach (SeatInfo seat in needReservingSeats)
            {
                _reservedSeatRepository.Save(new ReservedSeat
                                             {
                                                 Room = cinemaRoom,
                                                 RowNumber = seat.RowNumber,
                                                 SeatNumber = seat.SeatNumber,
                                                 ReservedTime = new DateTime()
                                             });
            }

            UpdateSeatMap(seatMap, needReservingSeats);
        }

        private void UpdateSeatMap(SeatMap seatMap, IList<SeatInfo> reservedSeats)
        {
            seatMap.MarkSeatReserved(reservedSeats);
            _cacheService.UpdateItem(seatMap.RoomName, seatMap);
        }
    }
}
