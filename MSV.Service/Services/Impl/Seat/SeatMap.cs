﻿using Microsoft.Extensions.Logging;
using MSV.Service.Models;
using ShareLib.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSV.Service.Services.Impl.Seat
{
    public class SeatMap
    {
        private static ILogger<SeatMap> logger = LoggerManager.CreateLogger<SeatMap>(); 

        private int allowedDistance;
        private SeatStatus[,] statusMatrix;
                
        public string RoomName { get; private set; }

        public SeatMap(CinemaRoom room)
        {
            this.RoomName = room.Name;
            this.allowedDistance = room.AllowedDistance;

            this.statusMatrix = new SeatStatus[room.Rows, room.SeatsPerRow];
            for (int i = 0; i < room.Rows; i++)
                for (int j = 0; j < room.SeatsPerRow; j++)
                    this.statusMatrix[i, j] = SeatStatus.AVAILABLE;
        }

        public void MarkSeatReserved(IList<SeatInfo> seats)
        {
            foreach (SeatInfo seat in seats)
                MarkSeatReserved(seat);
        }

        private void MarkSeatReserved(SeatInfo seat)
        {
            if (seat.RowNumber < 0 || seat.RowNumber >= statusMatrix.GetLength(0)
             || seat.SeatNumber < 0 || seat.SeatNumber >= statusMatrix.GetLength(1))
                return;

            if (statusMatrix[seat.RowNumber, seat.SeatNumber] != SeatStatus.AVAILABLE)
                return;

            statusMatrix[seat.RowNumber, seat.SeatNumber] = SeatStatus.RESERVED;

            int left = seat.SeatNumber - (allowedDistance - 1);
            int right = seat.SeatNumber + (allowedDistance - 1);
            int top = seat.RowNumber - (allowedDistance - 1);
            int bottom = seat.RowNumber + (allowedDistance - 1);
            MarkSeatsProhibit(left, right, top, bottom);
        }

        private void MarkSeatsProhibit(int left, int right, int top, int bottom)
        {

            top = top < 0 ? 0 : top;
            bottom = bottom >= statusMatrix.GetLength(0) ? statusMatrix.GetLength(0) - 1 : bottom;
            left = left < 0 ? 0 : left;
            right = right >= statusMatrix.GetLength(1) ? statusMatrix.GetLength(1) - 1 : right;

            for (int rowNumber = top; rowNumber <= bottom; rowNumber++)
                for (int seatNumber = left; seatNumber <= right; seatNumber++)
                    MarkSeatsProhibit(rowNumber, seatNumber);
        }

        private void MarkSeatsProhibit(int rowNumber, int seatNumber)
        {
            if (rowNumber < 0 || rowNumber >= statusMatrix.GetLength(0)
             || seatNumber < 0 || seatNumber >= statusMatrix.GetLength(1))
                return;

            if (statusMatrix[rowNumber, seatNumber] != SeatStatus.AVAILABLE)
                return;

            statusMatrix[rowNumber, seatNumber] = SeatStatus.PROHIBIT;
        }

        public IList<SeatInfo> GetAvailableSeats()
        {
            logger.LogInformation("Retrieving available seats");

            var availableSeats = new List<SeatInfo>();

            for (int i = 0; i < statusMatrix.GetLength(0); i++)
                for (int j = 0; j < statusMatrix.GetLength(1); j++)
                    if (statusMatrix[i, j] == SeatStatus.AVAILABLE)
                        availableSeats.Add(new SeatInfo
                                           {
                                               RowNumber = i,
                                               SeatNumber = j
                                           });

            return availableSeats;
        }

        public IList<SeatInfo> FetchUnavailableSeats(IList<SeatInfo> needReservingSeats)
        {
            if (needReservingSeats == null || needReservingSeats.Count <= 0)
                return needReservingSeats;

            return needReservingSeats.Where(s => statusMatrix[s.RowNumber, s.SeatNumber] != SeatStatus.AVAILABLE)
                                     .ToList();
        }
    }
}
