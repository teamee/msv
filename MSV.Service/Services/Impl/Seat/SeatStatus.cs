﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSV.Service.Services.Impl.Seat
{
    public enum SeatStatus : short
    {
        AVAILABLE, PROHIBIT, RESERVED
    }
}
