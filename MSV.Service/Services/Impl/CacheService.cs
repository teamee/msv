﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

namespace MSV.Service.Services.Impl
{
    /// <summary>
    /// NOTE: For demo purpose, this is a simple in-memory cache implementation.
    /// In production, this should use a high performance distributed cache such as Redis.
    /// </summary>
    public class CacheService : ICacheService
    {
        private readonly IDictionary<string, object> items = new ConcurrentDictionary<string, object>();

        public T GetItem<T>(string key)
        {
            return (T)items[key];
        }

        public void RemoveItem<T>(string key)
        {
            items.Remove(key);
        }

        public void UpdateItem<T>(string key, T item)
        {
            items[key] = item;
        }
    }
}
