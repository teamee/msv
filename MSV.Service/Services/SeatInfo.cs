﻿namespace MSV.Service.Services
{
    public class SeatInfo
    {
        public int RowNumber { get; set; }
        public int SeatNumber { get; set; }
    }
}
