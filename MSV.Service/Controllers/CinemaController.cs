﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MSV.Service.Exceptions;
using MSV.Service.Services;
using MSV.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSV.Service.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CinemaController : ControllerBase
    {        

        private readonly ILogger<CinemaController> _logger;
        private readonly ICinemaService _cinemaService;

        public CinemaController(ILogger<CinemaController> logger, ICinemaService cinemaService)
        {
            _logger = logger;
            _cinemaService = cinemaService;
        }

        [HttpPost]
        [Route("ConfigRoom")]
        public async Task<ResultResponse> ConfigRoom([FromBody] CinemaRoomConfigRequest configRequest)
        {
            try
            {
                _logger.LogInformation("ConfigRoom received input: ConfigRequest = {@Data}", configRequest);                

                await _cinemaService.ConfigCinemaRoom(configRequest.Name,
                                                      configRequest.Rows, configRequest.SeatsPerRow,
                                                      configRequest.AllowedDistance);

                _logger.LogInformation("ConfigRoom  succeeded.");

                return ResultResponse.Success();
            } 
            catch (CinemaException ce)
            {
                _logger.LogError(ce, "Error happened when configuring room");

                return ResultResponse.Failure(ce.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error happened when configuring room");

                return ResultResponse.Failure("Unknown error occurred.");
            }
        }

        [HttpGet]
        [Route("AvailableSeats")]
        public async Task<ActionResult<IEnumerable<Seat>>> GetAvailableSeats(string roomName, int neededSeatCount)
        {
            try
            {
                _logger.LogInformation("GetAvailableSeats received input: {@Data}", 
                                       new { RoomName = roomName, NeededSeatCount = neededSeatCount });                

                var availableSeats = await _cinemaService.GetAvailableSeats(roomName, neededSeatCount);

                return Ok(availableSeats.Select(s => new Seat
                                                     {
                                                         RowNumber = s.RowNumber,
                                                         SeatNumber = s.SeatNumber
                                                     })  
                                        .ToList());
            }
            catch (CinemaException ce)
            {
                _logger.LogError(ce, "Error happened when getting available seats");

                return StatusCode(StatusCodes.Status500InternalServerError, ce.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error happened when getting available seats");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [Route("ReserveSeats")]
        public async Task<ResultResponse> ReserveSeats([FromBody] ReserveSeatsRequest reserveRequest)
        {
            try
            {
                _logger.LogInformation("ReserveSeats received input: ReserveRequest = {@Data}", reserveRequest);

                await _cinemaService.ReserveSeats(reserveRequest.RoomName,
                                                  reserveRequest.Seats.Select(s => new SeatInfo
                                                                                   {
                                                                                       RowNumber = s.RowNumber,
                                                                                       SeatNumber = s.SeatNumber
                                                                                   })
                                                                      .ToList());

                _logger.LogInformation("ReserveSeats succeeded.");

                return ResultResponse.Success();
            }
            catch (CinemaException ce)
            {
                _logger.LogError(ce, "Error happened when reserving seats");

                return ResultResponse.Failure(ce.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error happened when reserving seats");

                return ResultResponse.Failure("Unknown error occurred.");
            }
        }
    }
}
