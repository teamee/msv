﻿using Microsoft.AspNetCore.Mvc;

namespace MSV.Service.Controllers
{
    [Route("api/[controller]")]
    public class HealthController : Controller
    {
        public IActionResult Get() => Ok("OK");
    }
}
