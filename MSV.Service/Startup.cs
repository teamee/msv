using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MSV.Service.Services.Impl;
using MSV.Service.Repository;
using MSV.Service.Repository.Impl;
using MSV.Service.Services;
using ShareLib.Logging;

namespace MSV.Service
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment enviroment)
        {
            var builder = new ConfigurationBuilder()
               .SetBasePath(enviroment.ContentRootPath)
               .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
               .AddJsonFile($"appsettings.{enviroment.EnvironmentName}.json", true)
               .AddEnvironmentVariables();
            Configuration = builder.Build();

            HostingEnvironment = enviroment;
        }

        public IConfiguration Configuration { get; }

        public IWebHostEnvironment HostingEnvironment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                    .AddNewtonsoftJson()
                    .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddSingleton<ICacheService, CacheService>();
            services.AddTransient<ICinemaService, CinemaService>();

            services.AddTransient<ICinemaRoomRepository, CinemaRoomRepository>();
            services.AddTransient<IReservedSeatRepository, ReservedSeatRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            LoggerManager.LoggerFactory = loggerFactory;

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
