﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSV.Service.Exceptions
{
    public class CinemaException : Exception
    {
        public CinemaException()
        {
        }

        public CinemaException(string message)
            : base(message)
        {
        }

        public CinemaException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
