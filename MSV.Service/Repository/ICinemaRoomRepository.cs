﻿using MSV.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSV.Service.Repository
{
    public interface ICinemaRoomRepository
    {
        CinemaRoom GetByName(string name);
        CinemaRoom Save(CinemaRoom cinemaRoom);
    }
}
