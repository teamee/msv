﻿using MSV.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSV.Service.Repository.Impl
{
    public class CinemaRoomRepository : BaseRepository, ICinemaRoomRepository
    {
        public CinemaRoom GetByName(string name)
        {
            return CINEMA_ROOMS.Values.Where(r => r.Name.Equals(name))
                                      .FirstOrDefault();
        }

        public CinemaRoom Save(CinemaRoom cinemaRoom)
        {
            if (cinemaRoom.Id <= 0)
                cinemaRoom.Id = NextCinemaRoomId();

            CINEMA_ROOMS[cinemaRoom.Id] = cinemaRoom;

            return cinemaRoom;
        }
    }
}
