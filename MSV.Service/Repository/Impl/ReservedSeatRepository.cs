﻿using MSV.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSV.Service.Repository.Impl
{
    public class ReservedSeatRepository : BaseRepository, IReservedSeatRepository
    {
        public void DeleteByRoom(CinemaRoom room)
        {
            var idsToRemove = RESERVED_SEATS.Where(spv => spv.Value.Room.Equals(room))
                                            .Select(spv => spv.Key)
                                            .ToList();
            foreach (var id in idsToRemove)
                RESERVED_SEATS.Remove(id);
        }

        public IList<ReservedSeat> FindByRoom(CinemaRoom room)
        {
            return RESERVED_SEATS.Values.Where(s => s.Room.Equals(room))
                                        .ToList();
        }

        public ReservedSeat Save(ReservedSeat reservedSeat)
        {
            if (reservedSeat.Id <= 0)
                reservedSeat.Id = NextReservedSeatId();

            RESERVED_SEATS[reservedSeat.Id] = reservedSeat;

            return reservedSeat;
        }
    }
}
