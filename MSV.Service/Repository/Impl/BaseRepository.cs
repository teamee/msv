﻿using MSV.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MSV.Service.Repository.Impl
{
    public class BaseRepository
    {
        protected static readonly IDictionary<int, CinemaRoom> CINEMA_ROOMS;
        protected static readonly IDictionary<long, ReservedSeat> RESERVED_SEATS;

        private static int INCREMENTAL_CINEMA_ROOM_ID = 0;
        private static long INCREMENTAL_RESERVED_SEATS_ID = 0;

        static BaseRepository()
        {
            CINEMA_ROOMS = new Dictionary<int, CinemaRoom>();
            RESERVED_SEATS = new Dictionary<long, ReservedSeat>();
        }

        protected int NextCinemaRoomId()
        {
            return Interlocked.Increment(ref INCREMENTAL_CINEMA_ROOM_ID);
        }

        protected long NextReservedSeatId()
        {
            return Interlocked.Increment(ref INCREMENTAL_RESERVED_SEATS_ID);
        }
    }
}
