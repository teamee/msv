# Microservice in dotnet core

This solution contains sample, patterns and best practices to build a microservice architecture using dotnet core. 

# Logging

Solution for Logging stack we build is EF2K (Elasticsearch - Fluent bit - Fluentd - Kibana). Architecture overview and detail of the stack are described in [Logging Aggregator](https://gitlab.com/nguyenminhdzung/logging-aggregator) repo.

In a microservice that deployed in container environment, we just write log entries to console and a log forwarder or a log aggregator will send it to log storage. In development environment, log entries might be also written to files for more convinience.

Dotnet core provide a logging framework and in our microservices, we use ILogger of dotnet core. The 3rd log provider can be Serilog or NLog... but it is done via configuration. That helps minimizing impact to our code when changing  log provider. In this solution, we demo the usage of [Serilog](https://serilog.net/).

**Inject ILogger**

    public class CinemaController : ControllerBase
    {        

        private readonly ILogger<CinemaController> _logger;

        public CinemaController(ILogger<CinemaController> logger)
        {
            _logger = logger;
        }
    }

**Write log**

    _logger.LogInformation("ConfigRoom  succeeded.");

**Config Serilog**

appsettings.json: Default write log to Console. Use ElasticsearchJsonFormatter because we use Elasticsearch for log storage. Some Enrichers for extra fields in log entries.

    {
        "Serilog": {
            "Using": [ "Serilog.Sinks.Console" ],
            "MinimumLevel": {
                "Default": "Information",
                "Override": {
                    "Microsoft": "Warning",
                    "System": "Warning"
                }
            },
            "Enrich": [ "FromLogContext", "WithMachineName", "WithThreadId", "WithThreadName" ],
            "WriteTo": [
                {
                    "Name": "Console",
                    "Args": {
                        "formatter": "Serilog.Formatting.Elasticsearch.ElasticsearchJsonFormatter, Serilog.Formatting.Elasticsearch"
                    }
                }
            ]
        }
    }

appsettings.Development.json: Write log to files for tracing in developer machine more convenient.

    "Serilog": {
        "Using": [ "Serilog.Sinks.Console" ],
        "MinimumLevel": {
            "Default": "Information",
            "Override": {
                "Microsoft": "Warning",
                "System": "Warning"
            }
        },
        "Enrich": [ "FromLogContext", "WithMachineName", "WithThreadId", "WithThreadName" ],
        "WriteTo": [
            {
                "Name": "Console"
            },
            {
                "Name": "File",
                "Args": {
                    "path": "logs/log-.txt",
                    "rollingInterval": "Day",
                    "shared": true,
                    "outputTemplate": "{Timestamp:dd/MM/yyyy HH:mm:ss.fff} [{Level:u5}] {MachineName} ({ThreadId}) {Message:lj}{NewLine}{Exception}"
                }
            }
        ]
    }

**ILogger for non-DI classes**

For global ILogger, use ShareLib.Logging.LoggerManager in ShareLib project.

Setup LoggerManager in Startup.cs

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
    {
        LoggerManager.LoggerFactory = loggerFactory;

        ...
    }

Declare and use it in a non-DI class

    public class SeatMap
    {
        private static ILogger<SeatMap> logger = LoggerManager.CreateLogger<SeatMap>(); 

        ...
    }

    public IList<SeatInfo> GetAvailableSeats()
    {
        logger.LogInformation("Retrieving available seats");

        ...
    }

**Write more data to extra fields in log entries**

Serilog provides Enrichers & Formaters for sending and presenting more data in log entries. But idea in our solution is not depending on specific features of logging providers. Fortunately, dotnetcore logging framework provide use way to place extra data to **placeholder** in log message template. By default, all logging provider de facto write value of ToString() of data in message template's placeholders and fortunately most of them write structured format of data in those placeholders that are prefixed with '@' character, e.g json serialize an object.

This code

    public async Task<ResultResponse> ConfigRoom([FromBody] CinemaRoomConfigRequest configRequest)
    {
        try
        {
            _logger.LogInformation("ConfigRoom received input: ConfigRequest = {@Data}", configRequest);

            ...
        }
    }

produces configRequest object in structured json format in **Data** field in the log entry

    {
        "@timestamp":"2021-08-28T18:34:21.5570958+07:00",
        "level":"Information",
        "messageTemplate":"ConfigRoom received input: ConfigRequest = {@Data}",
        "message":"ConfigRoom received input: ConfigRequest = CinemaRoomConfigRequest { Name: \"Cinema 1\", Rows: 50, SeatsPerRow: 30, AllowedDistance: 3 }",
        
        "fields":{
            "Data": {"_typeTag":"CinemaRoomConfigRequest",
                "Name":"Cinema 1",
                "Rows":50,
                "SeatsPerRow":30,
                "AllowedDistance":3
            },
            
            "SourceContext":"Service.Controllers.CinemaController",
            "ActionId":"c136116b-07a6-48cf-86e4-6fc108f3f6b2",
            "ActionName":"Service.Controllers.CinemaController.ConfigRoom (Service)",
            "RequestId":"80000021-0004-fd00-b63f-84710c7967bb",
            "RequestPath":"/api/cinema/ConfigRoom",
            "SpanId":"|f63a313-4ee0e252caebe52d.",
            "TraceId":"f63a313-4ee0e252caebe52d",
            "ParentId":"","MachineName":"LAPTOP-QB9SFCML",
            "ThreadId":11
        }
    }

That's it. Now we can send anything useful to extra fields in log entries and analyze them in Kibana or another logging visualizer.

# Deploy in Docker environment

In **docker-compose.yml**, just set logging driver to "fluentd" like this

    logging:
      driver: "fluentd"
      options:
        fluentd-async-connect: "true"
        tag: msv.Development.service

Logging data of the app will be handled by Logging Aggreagtor stack. See [Logging Aggregator Stack](https://gitlab.com/nguyenminhdzung/logging-aggregator) for more information.

# Unit Testing

TBD

# Configuration

TBD

# Tracing

TBD