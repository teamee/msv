﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShareLib.Logging
{
    public static class LoggerManager
    {
        public static ILoggerFactory LoggerFactory { get; set; }

        public static ILogger<T> CreateLogger<T>() => LoggerFactory.CreateLogger<T>();

        public static ILogger CreateLogger(string categoryName) => LoggerFactory.CreateLogger(categoryName);

        public static ILogger CreateLogger(Type type) => LoggerFactory.CreateLogger(type);
    }
}
