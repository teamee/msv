﻿using System;

namespace ShareLib.Utility
{
    public static class Guards
    {
        public static void MustBePositive(int value, String errorMessage)
        {
            if (value <= 0)
                throw new ArgumentException(errorMessage);
        }

        public static void NotNullOrEmpty(String value, String errorMessage)
        {
            if (String.IsNullOrWhiteSpace(value))
                throw new ArgumentException(errorMessage);
        }
    }
}
