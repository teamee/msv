using ShareLib.Utility;
using System;
using Xunit;

namespace ShareLibTest
{
    public class GuardsTest
    {
        [Theory]
        [InlineData(1)]
        [InlineData(10)]
        public void MustBePositive_Successful(int number)
        {
            Guards.MustBePositive(number, string.Empty);
            Assert.True(true);
        }

        private readonly string MUST_BE_POSITIVE_ERROR_MESSAGE = "Number must be positive";
        [Theory]
        [InlineData(-10)]
        [InlineData(-1)] 
        [InlineData(0)]
        public void MustBePositive_MustThrowException(int number)
        {
            var exception = Assert.Throws<ArgumentException>(() => Guards.MustBePositive(number, MUST_BE_POSITIVE_ERROR_MESSAGE));
            Assert.Equal(MUST_BE_POSITIVE_ERROR_MESSAGE, exception.Message);
                        
        }
    }
}
