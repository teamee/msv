dotnet tool install dotnet-reportgenerator-globaltool --tool-path test_tools

dotnet test /p:CollectCoverage=true /p:CoverletOutput=TestResults/ /p:CoverletOutputFormat=cobertura --logger:junit -r TestResults
test_tools\reportgenerator -reports:TestResults/coverage.cobertura.xml -targetdir:CoverageReport